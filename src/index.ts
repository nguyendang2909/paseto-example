import loggerFactory from './lib/logger-factory';
import paseto from 'paseto';
import crypto from 'crypto';

const logger = loggerFactory.getLogger(__filename);

const {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  V2: { encrypt, sign, verify },
} = paseto;

(async () => {
  const secretKey = await paseto.V2.generateKey('public');

  const symetrickey = '12345678901234567890123456789012';

  // console.log(secretKey);

  const fakeToken =
    'v2.local.AcCcXAbp6rSB5xJKa5lEuoTzsEMjsOc3V40T9HX1WDWEf1qovm7iPkGYQ-VNRkGvsFv_cv0HCkfyVjAscNj_0yRnI19cPEkb4640PlknA3_utfxDvl07PvsYeKHHURd_18-lor8qlN0iL7YXwzc1wcoOn5hw_HerFUTjBC8kZiLAC4nykMCcEHnbu2YKmBckH4mKMlDBbTbZ2rMWalN5d6sXfUbFASsIXpKRxx_5wwMeW-NPFXii0RgDP6NE-l1MDqGNS8XBcM8DgMEisSQ_fGmm3wMD4y2CNw.bnVsbA';

  await paseto.V2.verify(fakeToken, Buffer.from(symetrickey));
})();
